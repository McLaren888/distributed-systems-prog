# Lab 3

## Problem 1

In this problem, you will implement a **GraphQL** API on student object. A student has a student number, name and a collection of courses. A couse has a course code, a collection of assessment weights and corresponding marks.

Your API will implement queries on a student attributes as well as mutations to insert new courses.


## Problem 2

Create a gRPC implementation of a client and a server using a remote invocation to execute the following functionalities:

1. Create a new user;
1. View an existing user;
1. Update a field in an existing user;
1. Delete an existing user.

For the user creation operation, the sever will expect an object represented as follows:

```
{
	username: "billy",
	lastname: "Merlot",
	firstname: "William",
	email: "William.Merlot@adventurers.biz"
}
```
It returns a json object `{username: "billy"}` when the creation operation is successful.
As for the view and delete operations, the sever expects a json object with the _username_ and returns the complete user object in the first case and a response of successful deletion in the second or an error when one occurs. Finally, for the update operation, the server expects a json object containing the (unchanged username) and any of the other fields of the user object expect the username. When the operation completes a success or error message is returned.
